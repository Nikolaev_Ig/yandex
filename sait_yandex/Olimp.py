from flask import  Flask,render_template,request,session,logging,url_for,redirect,flash
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session,sessionmaker
from passlib.hash import sha256_crypt

engine = create_engine("mysql+pymysql://root:monvormo@localhost/register")
db = scoped_session(sessionmaker(bind=engine))
app = Flask(__name__)

@app.route("/")
@app.route("/home")
def home():
    return render_template("home.html")

@app.route("/login",methods=["GET","POST"])
def login():
    if request.method == "POST":
        email = request.form.get("email")
        password = request.form.get("password")

        emaildata = db.execute("SELECT email FROM users WHERE email=:email",{"email":email}).fetchone()
        passwordata = db.execute("SELECT password FROM users WHERE email=:email",{"email":email}).fetchone()


        if emaildata is None:
            return render_template("login.html")
        else:
            for password_data in passwordata:
                if sha256_crypt.verify(password,password_data):
                    session["log"] = True
                    flash("Приятного пользования","success")
                    return redirect(url_for("olimp"))
                else:
                    flash("Неверный пароль","danger")
                    return render_template("login.html")

    return  render_template("login.html")

@app.route("/register",methods=["GET","POST"])
def register():
    if request.method == "POST":
        name = request.form.get("name")
        email = request.form.get("email")
        password = request.form.get("password")
        confirm = request.form.get("confirm")
        secure_password = sha256_crypt.encrypt(str(password))

        if password ==confirm:
            db.execute("INSERT INTO users(name,email,password) VALUES(:name,:email,:password)",
                       {"name":name,"email":email,"password":secure_password})
            db.commit()
            flash("Вы успешно зарегистрировались","success")
            return redirect(url_for("login"))
        else:
            flash("Пароли не совпадают","danger")
            return render_template("register.html")
    return  render_template("register.html")


@app.route("/olimp")
def olimp():
    return render_template("olimp.html")

@app.route("/logout")
def logout():
    session.clear()
    return  redirect(url_for("login"))

@app.route("/infa")
def infa():
    return  render_template("infa.html")
@app.route("/fiz")
def fiz():
    return  render_template("fiz.html")
@app.route("/rus")
def rus():
    return  render_template("rus.html")
@app.route("/him")
def him():
    return  render_template("him.html")
@app.route("/bio")
def bio():
    return  render_template("bio.html")
@app.route("/matem")
def matem():
    return  render_template("matem.html")

if __name__ =="__main__":
    app.secret_key = "yandexlyceim123"
    app.run()